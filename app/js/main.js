/* jshint devel:true */

/* Sticky navigation panel */
(function (){
    var menuState = 'static';
    var navigationMenuSelector = '.l-navigation-menu';
    var logoNavigationMenuSelector = '.b-logo__navigation-menu';
    var menu,
        menuLogo;

    function initVariables() {
        if (!menu) {
            menu = document.querySelector(navigationMenuSelector);
        }
        if (!menuLogo) {
            menuLogo = document.querySelector(logoNavigationMenuSelector);
        }
    }

    window.onscroll = function() {
        var scrolledSize = document.documentElement.scrollTop || document.body.scrollTop;
        if (scrolledSize  > 78 && menuState === 'static') {
            menuState = 'fixed';
            initVariables();
            menu.style.position = 'fixed';
            menu.style.width = '100%';
            menu.style.top = '0';
            menu.style.zIndex = '999';
            menu.style.mozBoxShadow = '0px 4px 5px 0px rgba(0,0,0,0.15)';
            menu.style.webkitBoxShadow = '0px 4px 5px 0px rgba(0,0,0,0.15)';
            menu.style.boxShadow = '0px 4px 5px 0px rgba(0,0,0,0.15)';
            menuLogo.style.display = 'block';
        } else if (scrolledSize < 78 && menuState === 'fixed') {
            menuState = 'static';
            initVariables();
            menu.style.position = '';
            menu.style.width = '';
            menu.style.top = '';
            menu.style.zIndex = '';
            menu.style.mozBoxShadow = '';
            menu.style.webkitBoxShadow = '';
            menu.style.boxShadow = '';
            menuLogo.style.display = '';
        }
    };
}());

/* Responsive menu */
(function (){
    var menuItemsType = '';
    var menuState = 'hidden';

    /* Toggle menu items on sandwitch icon click */
    document.querySelector('.b-navigation-menu-item__sandwitch').onclick = function(e) {
        if (window.innerWidth < 750) {
            var i = 0;
            var menuHeight = 0;
            var items = document.querySelectorAll('.b-navigation-menu li');
            if (items[0].style.display === 'none' || items[0].style.display === '') {
                for (i = 0; items.length > i; i++) {
                    items[i].style.display = 'block';
                    menuHeight += items[i].offsetHeight;
                }
                menuState = 'visible';
                menuItemsType = 'block';
                document.querySelector('.l-navigation-menu').style.marginBottom = menuHeight+'px';
            } else {
                for (i = 0; items.length > i; i++) {
                    items[i].style.display = 'none';
                }
                menuState = 'hidden';
                menuItemsType = 'none';
                document.querySelector('.l-navigation-menu').style.marginBottom = '';
            }
        }
    };

    /* Hide menu on item click */
    var menuItems = document.querySelectorAll('.js-menu-item');
    for (var j = 0; menuItems.length > j; j++) {
        menuItems[j].onclick = function(e) {
                console.log('item click');
                if (window.innerWidth < 750) {
                    var i = 0;
                    var items = document.querySelectorAll('.b-navigation-menu li');
                    for (i = 0; items.length > i; i++) {
                        items[i].style.display = 'none';
                    }
                    menuState = 'hidden';
                    menuItemsType = 'none';
                    document.querySelector('.l-navigation-menu').style.marginBottom = '';
                }
            };
    }

    /* Set correct menu items type depend on browser width */
    window.onresize = function(e) {
        if (window.innerWidth > 750 && menuItemsType !== 'inline-block') {
            var i = 0;
            var items = document.querySelectorAll('.b-navigation-menu li');
            for (i = 0; items.length > i; i++) {
                items[i].style.display = '';
            }
            menuItemsType = 'inline-block';
            document.querySelector('.l-navigation-menu').style.marginBottom = '';
        }
        if (window.innerWidth < 750) {
            if (menuState === 'close' && menuItemsType !== 'none') {
                var i = 0;
                var items = document.querySelectorAll('.b-navigation-menu li');
                for (i = 0; items.length > i; i++) {
                    items[i].style.display = 'none';
                }
                menuItemsType = 'none';
            }
            if (menuState === 'open' && menuItemsType !== 'block') {
                var i = 0;
                var items = document.querySelectorAll('.b-navigation-menu li');
                for (i = 0; items.length > i; i++) {
                    items[i].style.display = 'block';
                }
                menuItemsType = 'block';
            }
        }
    };
}());



