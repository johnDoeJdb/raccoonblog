/*global -$ */
'use strict';
var gulp = require('gulp'),
    fileinclude = require('gulp-file-include'),
    sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
    gulp.src('./app/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./app/css'));
});

gulp.task('jshint', function() {
  return gulp.src('./app/js/**/*.js')
      .pipe(jshint())
      .pipe(jshint.reporter('fail'));
});

gulp.task('fileinclude', function() {
  gulp.src(['./app/html/index.html'])
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
      }))
      .pipe(gulp.dest('app/'));
});

gulp.task('build', ['jshint', 'sass', 'fileinclude'], function () {});

gulp.task('watch', function () {
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/html/**/*.html', ['fileinclude']);
});

gulp.task('default', ['build'], function () {});
